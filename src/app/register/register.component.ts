import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterService } from '../services/register.service'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap'
import Swall from 'sweetalert2'

interface register{
  name:string,
  phone:string,
  email:string,
  address:string,
  zip_code:string,
  build_cost:string,
  deposit:string,
  ein_file:File | null,
  city:string,
  seller_id:string,
  manufacture_id:string
  installation_date:any,
  confirmation_number:string,
  state_id:any
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})


export class RegisterComponent implements OnInit {

  register:register = {
    name:'',
    phone:'',
    email:'',
    address:'',
    zip_code:'',
    build_cost:'',
    deposit:'',
    ein_file: null,
    seller_id:'',
    manufacture_id: '',
    city: '',
    installation_date:{},
    confirmation_number:'',
    state_id: null
  };
  seller:any = {
    name:''
  };
  manofacture:any = {
    name:''
  }
  sellers:any = [];
  manofactures:any = [];
  states:any = [];
  isPrint:boolean = false;
  isEdit:boolean = false;
  onEdit:boolean = false;

  toast:any = Swall.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swall.stopTimer)
      toast.addEventListener('mouseleave', Swall.resumeTimer)
    }
  });
  currentOrder:number = 0;


  @ViewChild("Seller") Seller: TemplateRef<any> | undefined;
  @ViewChild("Manofacture") Manofacture: TemplateRef<any> | undefined;

  constructor(
    private registerService: RegisterService,
    private modalService: NgbModal,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.loadSellers();
    this.loadManofactures();
    this.loadState();
    this.loadEditRegister();

  }

  saveRegister(){
    this.isPrint = true;
    if(!this.validateDataOrder()){
      this.isPrint = false;
      return
    }
    const formData = this.createDataForm();
    this.registerService.saveRegister(formData)
    .subscribe(response => {
      if(response.success){
        this.isPrint = false;
        this.register = {
          name:'',
          phone:'',
          email:'',
          address:'',
          zip_code:'',
          build_cost:'',
          deposit:'',
          ein_file: null,
          seller_id:'',
          manufacture_id: '',
          city: '',
          installation_date:'',
          confirmation_number:'',
          state_id: null
        }
      }
      alert(response.message);
    });
  }
  uploadFile(event:any){
    this.register.ein_file = event.target.files[0];
  }
  createDataForm(){

    const formData:any = new FormData();
    formData.append('name',this.register.name);
    formData.append('phone',this.register.phone);
    formData.append('email',this.register.email);
    formData.append('address',this.register.city);
    formData.append('zip_code',this.register.zip_code);
    formData.append('build_cost',this.register.build_cost);
    formData.append('deposit',this.register.deposit);
    formData.append('ein_file',this.register.ein_file);
    formData.append('installation_date',this.register.installation_date.day+'-'+this.register.installation_date.month+'-'+this.register.installation_date.year);
    formData.append('seller_id',this.register.seller_id);
    formData.append('state_id',this.register.state_id);
    formData.append('manufacture_id',this.register.manufacture_id);
    formData.append('confirmation_number',this.register.confirmation_number);

    return formData;
  }

  loadSellers(){
    this.registerService.getSellers()
    .subscribe(response => {
      if(response.success){
        this.sellers = response.data;
      }
    });
  }
  loadManofactures(){
   this.registerService.getManofactures()
   .subscribe(response => {
    if(response.success){
      this.manofactures = response.data;
    }
   });
  }

  loadState(){
    this.registerService.getStates()
    .subscribe(response => {
      if(response.success){
        this.states = response.data;
      }
    });
  }

  openModal(content:any){
    this.modalService.open(content,{size:'md'}).result.then( result => {}).catch(res =>{});
  }

  saveSeller(){
    this.registerService.saveSeller(this.seller)
    .subscribe(response => {
      if(response.success){
        this.modalService.dismissAll();
        this.loadSellers();
      }
    })
  }

  saveManofacture(){
    this.registerService.saveManofacture(this.manofacture)
    .subscribe(response => {
      if(response.success){
        this.modalService.dismissAll();
        this.loadManofactures();
      }
    });
  }

  validateDataOrder(){
    if(this.register.name === ''){this.errorMessage('Name'); return false};
    if(this.register.phone === '') {this.errorMessage('Phone'); return false};
    if(this.register.city === '') {this.errorMessage('City'); return false};
    if(this.register.seller_id === '') {this.errorMessage('Sales Rep.'); return false};
    if(this.register.manufacture_id === '') {this.errorMessage('Manufacturer'); return false};
    if(this.register.installation_date === '') {this.errorMessage('Sales Date'); return false};
    if(this.register.build_cost === '') {this.errorMessage('Build Cost'); return false};
    if(this.register.deposit === '') {this.errorMessage('Deposit'); return false};
    if(this.register.state_id === '') {this.errorMessage('State'); return false};

    return true;
  }

  errorMessage(field:string){
    const Toast = Swall.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 4000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swall.stopTimer)
        toast.addEventListener('mouseleave', Swall.resumeTimer)
      }
    })

    Toast.fire({
      icon: 'error',
      title: 'The '+field +' is required'
    })
  }

  confirmDelete(msg:string,type:string,id:string){
    if(id != ''){
      Swall.fire({
        title: 'Delete '+msg+'?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          this.deleteItem(parseInt(id),type,msg);
        }
      })
    }else{
      this.toast.fire({
        icon: 'error',
        title: 'You need select a '+ msg
      })
    }

  }

  deleteItem(id:number, type:string,msg:string){
    switch (type) {
      case 'sl':
        this.deleteSalesman(id,msg)
        break;
      case 'mn':
        this.deleteManufacture(id,msg);
        break;
    }
  }

  deleteSalesman(id:number,msg:string){
    this.registerService.deleteSeller(id)
    .subscribe(
      response => {
        if(response.success){
          this.loadSellers();
          Swall.fire(
            'Deleted!',
            msg + ' has been deleted.',
            'success'
          )
        }
      }
    );
  }

  deleteManufacture(id:number,msg:string){
    this.registerService.deleteManufacture(id)
    .subscribe(response => {
      if(response.success){
        this.loadManofactures();
        Swall.fire(
          'Deleted!',
          msg + ' has been deleted.',
          'success'
        )
      }
    });
  }

  loadEditRegister(){
    let currentOrder = this.activatedRoute.snapshot.paramMap.get('id');
    if(currentOrder != null){
      this.isEdit = true;
      this.currentOrder = parseInt(currentOrder);
      this.registerService.getRegisterById(parseInt(currentOrder))
      .subscribe(response => {
        if(response.success){
          this.register = response.data;
          this.register.city = response.data.address
          this.register.installation_date = this.parseDateRegister(response.data.installation_date)
        }
      });
    }
  }

  editRegister(id:number){
    this.onEdit = true;
    this.register.installation_date = this.register.installation_date.day+'-'+this.register.installation_date.month+'-'+this.register.installation_date.year;    this.registerService.updateRegister(id, this.register)
    .subscribe(response => {
      if(response.success){
        Swall.fire(
          'Success!',
          'Register has been updated.',
          'success'
        )
        this.isEdit = false;
        this.router.navigate(['tracker/list'])
      }
    });
  }

  parseDateRegister(date:Date){

    let tempDate = new Date(date);
    return {
      'year': tempDate.getFullYear(),
      'month': tempDate.getMonth()+1,
      'day':tempDate.getDate()+1
    }
  }
}
