import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapRegisterComponent } from './map-register.component';

describe('MapRegisterComponent', () => {
  let component: MapRegisterComponent;
  let fixture: ComponentFixture<MapRegisterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapRegisterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
