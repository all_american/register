import { Component, OnInit } from '@angular/core';
import { Loader } from '@googlemaps/js-api-loader';
import { RegisterService } from '../services/register.service';

@Component({
  selector: 'app-map-register',
  templateUrl: './map-register.component.html',
  styleUrls: ['./map-register.component.scss']
})
export class MapRegisterComponent implements OnInit {

  loader = new Loader({
    apiKey: "AIzaSyAKXhW-9e4a_0Qxdrbt-ddt84CjbOMo15Q",
    version: "weekly",
    libraries: ["places"]
  });

  mapOptions = {
    center: {
      lat: 36.722983,
      lng: -95.402559
    },
    zoom: 4
  };

  orders:any = [];

  constructor(private registerService: RegisterService) { }

  ngOnInit(): void {
    this.getAllOrders();
    this.loadMapInfo();
  }

  loadMapInfo(){
    this.loader
    .load()
    .then((google) => {
      let map = new google.maps.Map(document.getElementById("map") as HTMLElement, this.mapOptions);
      setTimeout(() => {
        for (const order of this.orders) {
          let point = this.generateMark(order.address);
          point.then((res) => {
            const position: any = res;
            const infoWindow = new google.maps.InfoWindow({
              content: 'Build Cost: ' + order.build_cost
            });

            const marker = new google.maps.Marker({
              position: position,
              map: map
            });
            infoWindow.open({
              anchor: marker,
              map,
              shouldFocus: false
            })
          })
        }
      },2000);

    }).catch(e => {
      console.log(e);
    });
  }

  generateMark(address:string){
    let mark = new Promise((res, rej) => {
      this.loader.
      load()
      .then((google) => {
        let point = new google.maps.Geocoder;
        let mark = {lat:0,lng:0}
        point.geocode({address:address})
        .then((response => {
          let json = JSON.stringify(response,null,2);
          let result = JSON.parse(json);
          let lat = result.results[0].geometry.location.lat;
          let lng = result.results[0].geometry.location.lng;
          mark.lat = lat;
          mark.lng = lng;
          res(mark);
        }));
      }).catch((e) => {
        rej(e.message)
      });
    });

    return mark.then((res) => {
      return res;
    }).catch((e) => {
      return e;
    });
  }

  getAllOrders():any{
    this.registerService.getRegister()
    .subscribe(response => {
      if(response.success){
        this.orders = response.data;
      }
    });
  }

}
