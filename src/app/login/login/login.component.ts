import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swall from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  access:any = {
    user:'',
    password:''
  }

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  onLoggedin(){
    if(this.access.user === 'AmericanB' && this.access.password === 'americanb2023'){
      localStorage.setItem('isLoggedin','true');
      this.successMessage();
      this.router.navigate(['/tracker/main'])
    }
  }

  successMessage(){
    const Toast = Swall.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 2000,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swall.stopTimer)
        toast.addEventListener('mouseleave', Swall.resumeTimer)
      }
    })

    Toast.fire({
      icon: 'success',
      title: 'Signed in successfully'
    })
  }

}
