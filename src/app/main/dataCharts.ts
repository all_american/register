
const ChartC:any = {
  series: [],
  chart: {
    height: 350,
    type: "bar",
    events: {
      click: function(chart:any, w:any, e:any) {
        // console.log(chart, w, e)
      }
    }
  },
  colors: [
    "#008FFB",
    "#00E396",
    "#FEB019",
    "#FF4560",
    "#775DD0",
    "#546E7A",
    "#26a69a",
    "#D10CE8"
  ],
  plotOptions: {
    bar: {
      columnWidth: "45%",
      distributed: true
    }
  },
  dataLabels: {
    enabled: false
  },
  legend: {
    show: false
  },
  grid: {
    show: false
  },
  xaxis: {
    categories: ['January', 'Febraury', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    labels: {
      style: {
        colors: [
          "#008FFB",
          "#00E396",
          "#FEB019",
          "#FF4560",
          "#775DD0",
          "#546E7A",
          "#26a69a",
          "#D10CE8",
          "#008FFB",
          "#00E396",
          "#FEB019",
          "#FF4560"
        ],
        fontSize: "12px"
      }
    }
  }
};

const ChartD:any = {
  series: [],
  chart: {
    width: 500,
    type: "pie"
  },
  labels: ["Build Cost", "Total Deposit", "Difference"],
  responsive: [
    {
      breakpoint: 480,
      options: {
        chart: {
          width: 200
        },
        legend: {
          position: "bottom"
        }
      }
    }
  ]
};

export {ChartC, ChartD};
