import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../services/register.service';
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexYAxis,
  ApexDataLabels,
  ApexGrid,
  ApexPlotOptions,
  ApexLegend,
  ApexNonAxisChartSeries,
  ApexResponsive,
 } from 'ng-apexcharts';
 import { ChartD, ChartC } from './dataCharts';
import * as moment from 'moment';

 export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  yaxis: ApexYAxis;
  xaxis: ApexXAxis;
  grid: ApexGrid;
  colors: string[];
  legend: ApexLegend
};

export type ChartPieOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
}

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public chartC: Partial<ChartOptions> | any;
  public chartD: Partial<ChartPieOptions> | any;


  estadistic:any = [];
  constructor(private registerService: RegisterService) {
    this.chartC = ChartC;
    this.chartD = ChartD;
    this.loadListOfOrders();
  }

  ngOnInit(): void {

  }

  loadListOfOrders(){
    this.registerService.getRegister()
    .subscribe(response => {
      if(response.success){
        this.estadistic = response.data;
        this.generateCostByMonth(response.data)
        this.generateCostOnCurrentMonth(response.data)
      }
    });
  }

  currencyFormatter({ currency, value}:any) {
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      minimumFractionDigits: 2,
      currency
    })
    return formatter.format(value)
  }

  calculateTotals(type:string, data:any = null):string{
    let temp_cost = 0;
    let temp_deposit = 0;
    let result:any;
    let estadistic:any = (data === null) ? this.getSalesToday() : data;

    for (const order of estadistic) {
        let cost = parseFloat(order.build_cost);
        let deposit = parseFloat(order.deposit);
        temp_cost = temp_cost + ((!cost) ? 0 : cost);
        temp_deposit = temp_deposit + ((!deposit) ? 0 : deposit);
    }

    switch (type) {
      case 'cost':
        result =  (data === null) ? this.currencyFormatter({currency: 'USD', value: temp_cost}) : temp_cost;
        break;
      case 'deposit':
        result =  (data === null) ? this.currencyFormatter({currency: 'USD', value: temp_deposit}) : temp_deposit;
        break;
      case 'diff':
        result =  (data === null) ? this.currencyFormatter({currency: 'USD', value: (temp_cost - temp_deposit)}) : (temp_cost - temp_deposit)
        break;
    }
    return result;
  }

  getSalesToday(){
    return this.estadistic.filter((item:any) => {
      let currentDate = moment(item.created_at).format('L');
      let today = moment().format('L');
      if(currentDate === today) return item;
    });
  }

  getSalesMonth(month:number, data:any){
    let orderByMonth = data.filter((order:any) => {
      let monthOrder = moment(order.created_at).month() + 1;
      if(monthOrder === month) return order;
    });

    if(orderByMonth.lenght === 0) {
      return 0;
    }

    let cost = 0;
    for (const order of orderByMonth) {
      let build = parseFloat(order.build_cost);
      cost = cost + ((!build)? 0 : build);
    }
    return cost;
  }

  generateCostByMonth(data:any){
    let currentMonth = moment().month()+1;
    let dataChart:any = [];
    for(let i = 1; i <= currentMonth; i++){
      dataChart.push(this.getSalesMonth(i, data))
    }
    this.chartC.series = [
      {
        name: "Total Build Cost",
        data: dataChart
      }
    ];

  }

  generateCostOnCurrentMonth(data:any){
    let currentMonth = moment().get('month');
    let orders = data.filter((order:any) => {
      let month = moment(order.created_at).get('month');
      if(currentMonth === month) return order;
    });
    let cost = this.calculateTotals('cost', orders);
    let deposit = this.calculateTotals('deposit', orders);
    let diff = this.calculateTotals('diff', orders)
    this.chartD.series = [cost,deposit,diff];
  }

}
