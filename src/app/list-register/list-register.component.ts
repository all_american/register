import { Component, OnInit, ViewChild  } from '@angular/core';
import { RegisterService } from '../services/register.service';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import * as moment from 'moment';
import Swall from 'sweetalert2'

@Component({
  selector: 'app-list-register',
  templateUrl: './list-register.component.html',
  styleUrls: ['./list-register.component.scss']
})
export class ListRegisterComponent implements OnInit {

  @ViewChild(DatatableComponent) table: DatatableComponent | undefined;
  orders:any = [];
  ColumnMode:any = ColumnMode;
  tempOrders:any = [];
  filterOrders:any = [];
  total_cost:string = '';
  total_deposit:string = '';
  total_diference:string = '';
  states:any = [];
  sellers:any = [];
  manofactures:any = [];
  filter:any = {
    state:'',
    manufacture:'',
    seller:'',
    start_date:null,
    end_date:null
  };
  currentOrder:any = {};

  toast:any = Swall.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 2000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swall.stopTimer)
      toast.addEventListener('mouseleave', Swall.resumeTimer)
    }
  })

  flagUpdate:boolean = false;

  constructor(
    private registerService: RegisterService,
    private modalService: NgbModal,
    private router: Router) {
    this.loadState();
    this.loadSellers();
    this.loadManofactures();
   }

  ngOnInit(): void {
    this.loadListOfOrders();
  }

  loadListOfOrders(){
    this.registerService.getRegister()
    .subscribe(response => {
      if(response.success){
        this.orders = response.data;
        this.filterOrders = response.data;
        this.tempOrders = response.data;
      }
    });
  }

  deleteRegister(register:number){
    this.registerService.deleteRegister(register)
    .subscribe(response => {
      if(response.success){
        this.loadListOfOrders();
        let message = response.message;
        Swall.fire(
          'Deleted!',
            message,
          'success'
        )
      }
    });
  }

  confirmDelete(register:number){
    Swall.fire({
      title: 'Are you sure delete register?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteRegister(register)
      }
    })
  }

  filterAction(event:any, type:number){
    const filter = event.name.toLowerCase();
    const temp = this.orders.filter((order:any) => {
      let result;
      switch (type) {
        case 1:
          result =  order.state.name.toLowerCase().indexOf(filter) !== -1 || !filter;
          break;
        case 2:
          result =  order.manufacture.name.toLowerCase().indexOf(filter) !== -1 || !filter;
          break;
        case 3:
          result =  order.seller.name.toLowerCase().indexOf(filter) !== -1 || !filter;
          break;
      }
      return result;
    });

    this.orders = temp;
  }

  dateStarFilter(event:any){
    let dateSelected = event.year+'/'+event.month+'/'+event.day;
    const temp = this.orders.filter((order:any) => {
      let orderDate = this.formatForDate(order.installation_date, true);
      if(orderDate === dateSelected){
        return order;
      }
    });
    this.orders = temp;
  }
  dateEndFilter(event:any){
    if(this.filter.start_date != ''){
      let start = moment(this.filter.start_date.year+'-'+this.filter.start_date.month+'-'+this.filter.start_date.day).format('L');
      let end = moment(event.year+'-'+event.month+'-'+(event.day)).format('L');;
      const temp = this.filterOrders.filter((order:any)=>{
        let orderDate = moment(order.installation_date).format('L');;
        if(orderDate >= start){
          if(orderDate <= end){
            return order;
          }
        }
      });
      this.orders = temp;
    }
  }

  clearFilter(){
    this.orders = this.tempOrders;
    this.filter.state = '';
    this.filter.manufacture = '';
    this.filter.seller = '';
    this.filter.start_date = null;
    this.filter.end_date = null;
  }

  calculateDiference(cost:string,deposit:string){
    let tempCost = parseFloat(cost);
    let tempDeposit = parseFloat(deposit);
    let result = tempCost - tempDeposit;
    return this.currencyFormatter({currency: 'USD', value: result});
  }

  currencyFormatter({ currency, value}:any) {
    const formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      minimumFractionDigits: 2,
      currency
    })
    return formatter.format(value)
  }

  updateStatus(id:any){
    this.registerService.updateStatus(id)
    .subscribe(response => {
      if(response.success){
        alert(response.message);
      }
    });
  }
  updateNotesRegister(){
    this.flagUpdate = true;
    this.registerService.updateRegister(this.currentOrder.id, this.currentOrder)
    .subscribe(response => {
      if(response.success){
        this.flagUpdate = false;
        Swall.fire(
          'Success!',
          'Note has been saved.',
          'success'
        )
        this.modalService.dismissAll()
      }
    });
  }

  updateRegister(id:number){
    this.router.navigate(['tracker/register/'+id])
  }

  calculateTotals(type:string):string{
    let temp_cost = 0;
    let temp_deposit = 0;
    let result = '';

    for (const order of this.orders) {
        temp_cost = temp_cost + parseFloat(order.build_cost);
        temp_deposit = temp_deposit + parseFloat(order.deposit);
    }

    switch (type) {
      case 'cost':
        result =  this.currencyFormatter({currency: 'USD', value: temp_cost});
        break;
      case 'deposit':
        result =  this.currencyFormatter({currency: 'USD', value: temp_deposit});
        break;
      case 'diff':
        result =  this.currencyFormatter({currency: 'USD', value: (temp_cost - temp_deposit)})
        break;
    }
    return result;
  }

  calculateTotalCost(){
    return this.calculateTotals('cost')
  }

  calculateTotalDeposit(){
    return this.calculateTotals('deposit');
  }

  calculateTotalDiff(){
    return this.calculateTotals('diff');
  }

  formatForDate(date:Date,converter:boolean = false){
    const tempDate = new Date(date);
    if(converter){
      return  tempDate.getFullYear() + "/" + ((tempDate.getMonth()+1 > 9) ? tempDate.getMonth()+ 1 : ('0' + (tempDate.getMonth() + 1))) + "/" + (tempDate.getDate() > 10 ? tempDate.getDate() : '0' + tempDate.getDate());
    }
    return ((tempDate.getMonth()+1 > 9) ? tempDate.getMonth()+ 1 : ('0' + (tempDate.getMonth() + 1))) + "-" + (tempDate.getDate() > 10 ? tempDate.getDate() : '0' + tempDate.getDate()) + "-" + tempDate.getFullYear().toString().substring(2,4);
  }

  includeStateOnMainList(state_id:number){
    let stateFound = this.states.find((state:any)=>{
      return state.id === state_id
    });
    if(stateFound === undefined){
      return '';
    }
    return stateFound.name;
  }

  //Load Catalogs
  loadState(){
    this.registerService.getStates()
    .subscribe(response => {
      if(response.success){
        this.states = response.data;
      }
    });
  }
  loadSellers(){
    this.registerService.getSellers()
    .subscribe(response => {
      if(response.success){
        this.sellers = response.data;
      }
    });
  }
  loadManofactures(){
   this.registerService.getManofactures()
   .subscribe(response => {
    if(response.success){
      this.manofactures = response.data;
    }
   });
  }

  //Modal
  openModal(content:any){
    this.modalService.open(content,{size:'md'}).result.then( result => {}).catch(res =>{});
  }

  selectCurrentOrder(modal:any, order:number){
    this.registerService.getRegisterById(order)
    .subscribe(response => {
      if(response.success) {
        this.currentOrder = response.data;
        this.openModal(modal);
      }
    })
  }

  findElement(id:number, items:[]){
    let found:any = items.find((item:any) => item.id === id);
    return found.name;
  }

}
