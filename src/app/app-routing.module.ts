import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { MapRegisterComponent } from './map-register/map-register.component';
import { ListRegisterComponent } from './list-register/list-register.component';
import { LoginComponent } from './login/login/login.component';
import { DashComponent } from './dash/dash.component';
import { AuthGuard } from './guards/auth.guard';
import { MainComponent } from './main/main.component';


const routes: Routes = [
  {
    path: '',
    component:LoginComponent
  },
  {
    path: 'tracker',
    component: DashComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path:'main',
        component: MainComponent
      },
      {
        path: 'register',
        component:RegisterComponent
      },
      {
        path: 'register/:id',
        component:RegisterComponent
      },
      {
        path: 'list',
        component:ListRegisterComponent
      }
    ]
  },

];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [RegisterComponent,MapRegisterComponent,ListRegisterComponent];
