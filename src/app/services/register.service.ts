import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  httpOptions:any = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'POST',
      'Access-Control-Allow-Origin': '*'
    })
  }
  constructor(private http:HttpClient) { }

  saveRegister(register:any):Observable<any>{
    return this.http.post("https://bapi.allamericanmbs.com/buildings-api/public/api/save-register", register,this.httpOptions);
  }
  saveSeller(seller:any):Observable<any>{
    return this.http.post("https://bapi.allamericanmbs.com/buildings-api/public/api/sellers", seller,this.httpOptions);
  }
  saveManofacture(manofacture:any):Observable<any>{
    return this.http.post("https://bapi.allamericanmbs.com/buildings-api/public/api/manufactures", manofacture,this.httpOptions);
  }

  deleteRegister(order:number):Observable<any>{
    return  this.http.delete("https://bapi.allamericanmbs.com/buildings-api/public/api/registers/"+ order);
  }

  //Gets

  getRegister():Observable<any>{
    return this.http.get("https://bapi.allamericanmbs.com/buildings-api/public/api/registers");
  }

  getRegisterById(order:number):Observable<any>{
    return this.http.get("https://bapi.allamericanmbs.com/buildings-api/public/api/registers/"+ order);
  }

  getSellers():Observable<any>{
    return this.http.get("https://bapi.allamericanmbs.com/buildings-api/public/api/sellers");
  }
  getManofactures():Observable<any>{
    return this.http.get("https://bapi.allamericanmbs.com/buildings-api/public/api/manufactures");
  }

  getStates():Observable<any>{
    return this.http.get("https://bapi.allamericanmbs.com/buildings-api/public/api/states");
  }

  updateStatus(order:any):Observable<any>{
    return this.http.get("https://bapi.allamericanmbs.com/buildings-api/public/api/registers-status/"+order);
  }

  updateRegister(id:number, register:any):Observable<any>{
    return this.http.put("https://bapi.allamericanmbs.com/buildings-api/public/api/registers/"+id,register);
  }

  deleteSeller(seller:number):Observable<any>{
    return this.http.delete("https://bapi.allamericanmbs.com/buildings-api/public/api/sellers/"+seller);
  }

  deleteManufacture(manofacture:number):Observable<any>{
    return this.http.delete("https://bapi.allamericanmbs.com/buildings-api/public/api/manufactures/"+manofacture);
  }

}
