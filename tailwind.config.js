/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html,ts}",
    "./node_modules/flowbite/**/*.js"
  ],
  theme: {
    extend: {
      fontFamily:{
        'inter':'Inter'
      },
    },
    colors:{
      'american-red': '#8a172b',
      'american-blue': '#262262',
      'american-white': '#fff'
    }
  },
  plugins: [
    require('flowbite/plugin')
],
}
